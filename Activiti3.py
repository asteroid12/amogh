# Activity 3

def input_two_numbers():
    n1 = int(input("Enter the first number: "))
    n2 = int(input("Enter the second number: "))
    return (n1, n2)

def add(n1, n2):
    return n1 + n2

def output(n1, n2, total):
    print(f"Sum of {n1} and {n2} is {total}")
    
def main():
    n1, n2 = input_two_numbers()
    total = add(n1, n2)
    output(n1, n2, total)
    
main() 