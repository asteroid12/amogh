def get_cs():
    s = input("Enter the concatenated string: ")
    return s
    
def cs_to_dict(ld):
    d={i[0]:i[1] for i in [tuple(i.split('='))for i in ld.split(";")]}
    return d
    
def dict_to_cs(d):
    cs=';'.join(['='.join(i) for i in d.items()])
    return cs

def main():
    cs=get_cs()
    d=cs_to_dict(cs)
    print(f'\nS to d : \n{d}')
    cs=dict_to_cs(d)
    print(f'\nD to S :\n{cs}')

main()
