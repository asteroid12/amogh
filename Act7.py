class Menu:

    def __init__(self):
        self.menu = {}

    def add(self, food, cost):
        self.menu[food] = cost

    def show(self):
        for food, cost in self.menu.items():
            print(food, cost)

def main():
    m = Menu()
    m.add("idly", 10)
    m.add("vada", 20)
    m.show()

main()